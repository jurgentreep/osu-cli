# osu!cli Beatmap Downloader

osu!cli is a tool to help you subscribe to osu! mappers and download their beatmaps through mirrors and the official website.

You can specify a list of mappers' usernames. The program will grab the list of all his/her beatmaps, then it will iterate through mirror websites (Bloodcat, loli.ol, osu.uu.gl, and osu.ppy.sh) to find the first available link and then download it.

You can also specify your `Songs` where osu stores all your beatmaps. The program will check if the beatmap exists in the folder, and skip it if it does. You also have the option to toggle duplication check, and write your own filter in the code.

Usage
--------

You can follow the instructions below or read the [walkthrough](http://slides.com/rix/osu-cli).

1. Download and extract the [latest release](https://bitbucket.org/osudev/osu-cli/get/master.zip), or clone with git
2. Run `start.cmd` in the extracted folder and follow the instructions

TODOs
--------

- [x] When an unavailable beatmap set is seen, add it into a list stored on the disk. In the next time the list is used to filter out the candidates. A switch in the config toggles this feature.
- [ ] Use a more elegant progress bar.
- [ ] i18n.
- [ ] Implement download resume functionality.
- [ ] Implement parallel downloads.
- [ ] Implement speed limitation.
- [ ] Refactor the download services into modular components.
- [ ] Replace async with promise.
- [ ] Create a GUI wrapper.
