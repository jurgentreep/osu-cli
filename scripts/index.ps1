﻿Function My-Join-Path ($Base, $Relative) {
    return [System.IO.Path]::GetFullPath((Join-Path $Base $Relative));
}

$ROOT_DIR = My-Join-Path $PSScriptRoot "..";
$LIB_DIR = My-Join-Path $ROOT_DIR "lib";
$DATA_DIR = My-Join-Path $ROOT_DIR "data";
$VENDOR_DIR = My-Join-Path $ROOT_DIR "vendor";
$NODE_MODULES_DIR = My-Join-Path $ROOT_DIR "node_modules";

$CONFIG_FILE = My-Join-Path $DATA_DIR "config.ls";
$USER_LIST_FILE = My-Join-Path $DATA_DIR "user_list.txt";
$CONFIG_SAMPLE_FILE = My-Join-Path $LIB_DIR "config.sample.ls";
$USER_LIST_SAMPLE_FILE = My-Join-Path $LIB_DIR "user_list.sample.txt";

$NODE = 'node.exe';
$NPM = 'npm.cmd';
$NODE_VERSION = [Version]'0.10.0';
$ARCH = $ENV:PROCESSOR_ARCHITECTURE;
$NODE_URL = @{
    "x86" = "http://nodejs.org/dist/v0.10.35/node-v0.10.35-x86.msi";
    "amd64" = "http://nodejs.org/dist/v0.10.35/x64/node-v0.10.35-x64.msi"
};

$TEXT_EDITOR = My-Join-Path $VENDOR_DIR "notepad2/notepad2.exe";
$TEXT_EDITOR_DIR = My-Join-Path $VENDOR_DIR "notepad2";
$TEXT_EDITOR_URL = "http://www.flos-freeware.ch/zip/notepad2_4.2.25_x86.zip";

$CONSOLE_TITLE = "osu!cli Beatmap Downloader";

$host.ui.RawUI.WindowTitle = $CONSOLE_TITLE;

Function Dev-Clean {
    Clear-Host;
    Write-Host "Cleaning up...";
    Remove-Directory $DATA_DIR, $VENDOR_DIR, $NODE_MODULES_DIR;
}

Function Check-File-Structure {
    Set-Location $ROOT_DIR;
    Make-Directory $DATA_DIR, $VENDOR_DIR;
}

Function Check-Node {
    Function Node-Exists {
        if (If-Command-Exist $NODE) {
            return $true;
        }
        if (Yes-Or-No -Prompt "Node.js was not found. Do you want to download and install it now?" -Y) {
            Install-Node;
            if (If-Command-Exist $NODE) {
                return $true;
            } else {
                Write-Host "Your need to reboot your machine to complete the Node.js installation process.";
                if (Yes-Or-No -Prompt "Do you want to reboot your machine now?") {
                    Restart-Computer -Confirm;
                }
            }
        }
        return $false;
    }

    Function Node-Valid {
        $NodeVersion = [Version](gci (Which $NODE)).VersionInfo.ProductVersion;
        if ($NodeVersion -ge $NODE_VERSION) {
            return $true;
        }
        Write-Host "Your Node.js version $NodeVersion is not supported.";
        Write-Host "Minimum version of $NODE_VERSION is required."
        if (Yes-Or-No -Prompt "Do you want to download and update to the latest Node.js version?" -Y) {
            Install-Node;
            return Node-Valid;
        }
        return $false;
    }

    Function Npm-Exists {
        if (If-Command-Exist $NPM) {
            return $true;
        }
        Write-Host "It seems like your Node.js installation is broken. (NPM not found)";
        if (Yes-Or-No -Prompt "Do you want to download and re-install Node.js?" -Y) {
            Install-Node;
            return Check-Node;
        }
        return $false;
    }

    (Node-Exists) -and (Node-Valid) -and (Npm-Exists);
}

Function Check-Configs {
    Function Init-Configs {
        Clear-Host;
        Write-Host "Initializing settings...";
        Copy-Item $CONFIG_SAMPLE_FILE $CONFIG_FILE;
        Configure-Api-Key;
        if (Configure-Osu-Service) {
            Configure-Osu-Login;
        }
        Configure-Osu-Dir;
        Configure-Download-Dir;
    }

    Function Configure-Api-Key {
        Write-Host "The program requires an osu! API key.";
        Write-Host "You may apply one through https://osu.ppy.sh/p/api";
        if (Yes-Or-No "Do you want to open the website now?" -Y) {
            Start-Process "https://osu.ppy.sh/p/api";
        }
        $Key = Read-Host "Right click and paste your API key here, then press ENTER";
        Replace-Text $CONFIG_FILE "THE_API_KEY" $Key;
    }

    Function Configure-Osu-Service {
        Clear-Host;
        Write-Host "The osu! official download service needs your osu! login username and password.";
        Write-Host "If the service is enabled, it will either read the authentication data from the config file if it's set in this process, or it will ask you to type in every time if you leave them blank.";
        if (Yes-Or-No "Do you want to enable official osu! download service?") {
            Replace-Text $CONFIG_FILE "# 'osu'" "'osu'";
            return $true;
        }
        return $false
    }

    Function Configure-Osu-Login {
        Write-Host "Please enter your osu! login data.";
        $Username = Read-Host "Username";
        $Password = [Runtime.InteropServices.Marshal]::PtrToStringAuto(
                    [Runtime.InteropServices.Marshal]::SecureStringToBSTR((
                    Read-Host 'Password (Hidden)' -AsSecureString)));
        Replace-Text $CONFIG_FILE "THE_USERNAME" $Username;
        Replace-Text $CONFIG_FILE "THE_PASSWORD" $Password;
    }

    Function Configure-Osu-Dir {
        Clear-Host;
        Write-Host "In order to filter out the installed beatmaps, the program needs your osu! installation directory.";
        Write-Host "For example: D:\osu!";
        $Path = (Read-Host "The osu! installation directory") -replace "\\", "\\";
        Replace-Text $CONFIG_FILE "THE_OSU_DIR" $Path;
        Replace-Text $CONFIG_FILE "THE_SONGS_DIR" "$Path\\Songs";
    }

    Function Configure-Download-Dir {
        Clear-Host;
        Write-Host "Please specify a directory to save the downloaded beatmaps. You can just use the Songs folder in the osu! installation directory in order to make the downloaded beatmaps auto installed, but it may be buggy, so it's better to use a separate folder.";
        Write-Host "For example: E:\Downloads\Beatmaps";
        $Path = (Read-Host "The download folder") -replace "\\", "\\";
        Replace-Text $CONFIG_FILE "THE_DOWNLOAD_DIR" $Path;
    }

    if (!(Test-Path $CONFIG_FILE)) {
        Init-Configs;
    }
}

Function Check-User-List {
    Function Init-User-List {
        Clear-Host;
        Write-Host "Initializing user list...";
        Copy-Item $USER_LIST_SAMPLE_FILE $USER_LIST_FILE;
        Write-Host "The user list is the place you put all your favorite mappers. The program will walk through the list and grab their latest beatmap releases.";
        if (Yes-Or-No "Do you want to edit the user list now?" -Y) {
            Edit-Text-File $USER_LIST_FILE;
        }
    }

    if (!(Test-Path $USER_LIST_FILE)) {
        Init-User-List;
    }
    
}

Function Check-Node-Modules {
    if (!(Test-Path $NODE_MODULES_DIR)) {
        Clear-Host;
        Write-Host "Installing npm modules...";
        Start-Process -FilePath (Which $NPM) -ArgumentList "install" -NoNewWindow -Wait;
    }
}

Function Main-Menu {
    Function Create-Menu ([string]$Title, [array]$Options, [string]$Default = "") {
        Clear-Host;
        Write-Host $Title;
        Write-Host "";
        $Options | % {$i=1} {
            Write-Host "  [$i] $_";
            $i++
        }
        Write-Host "  [Q] Quit or go back";
        while($true) {
            Write-Host -NoNewline "`nType your choice and press Enter";
            if ($Default) {
                Write-Host -NoNewline " (Default is $Default)";
            }
            Write-Host -NoNewline ": ";
            $Answer = Read-Host;
            If (!$Answer) {$Answer = $Default}
            if ($Answer -eq "q") {
                return $false;
            }
            Try {
                if($Answer) {
                    $i = $Answer - 1;
                    if ($Options[$i++]) {
                        return $i;
                    }
                }
            }
            Catch [System.Exception] {}
            Write-Host "Invalid input. Please tyr again.";
        }
    }

    while($true) {
        $MainMenuOptions = @(
            "Start download",
            "Edit config file",
            "Edit user list file",
            "Open the download folder"
        );
        $Answer = Create-Menu -Title "Welcome to osu!cli Beatmap Downloader!" -Options $MainMenuOptions -Default 1;
        Switch ($Answer) {
            1 { # Start download
                Start-Process -FilePath (Which $NODE) -ArgumentList $ROOT_DIR -NoNewWindow -Wait;
                pause;
            }
            2 { # Edit config file
                Edit-Text-File $CONFIG_FILE;
            }
            3 { # Edit user list file
                Edit-Text-File $USER_LIST_FILE;
            }
            4 { # Open the download folder
                $pinfo = New-Object System.Diagnostics.ProcessStartInfo;
                $pinfo.FileName = (Which $NODE);
                $pinfo.Arguments =
                    (My-Join-Path $NODE_MODULES_DIR "LiveScript\bin\lsc"),
                    "$LIB_DIR\get_config.ls",
                    "download_dir";
                $pinfo.UseShellExecute = $false;
                $pinfo.CreateNoWindow = $true;
                $pinfo.RedirectStandardOutput = $true;
                $pinfo.RedirectStandardError = $true;
                $process = New-Object System.Diagnostics.Process;
                $process.StartInfo = $pinfo;
                $process.Start() | Out-Null;
                $process.WaitForExit();
                $stdout = $process.StandardOutput.ReadToEnd()
                $dirpath = $stdout -replace "`n|`r";
                ii $dirpath;
            }
            Default { # Quit
                return;
            }
        }
    }
}

Function Edit-Text-File ($Filepath) {
    if (Test-Path $TEXT_EDITOR) {
        Write-Host "Editing $Filepath...";
        Start-Process -FilePath $TEXT_EDITOR -ArgumentList $Filepath -Wait;
        return;
    }
    if (Yes-Or-No -Prompt "Text editor not found. Do you want to install one?" -Y) {
        Install-Text-Editor;
        Edit-Text-File $Filepath;
    }
}

Function Install-Node {
    Clear-Host;
    Write-Host "Installing Node.js...";
    $Url = $NODE_URL[$ARCH];
    $Filepath = My-Join-Path $VENDOR_DIR ([System.IO.Path]::GetFileName($Url));
    Download $Url $Filepath;
    Start-Process -FilePath $Filepath -Wait;
    Remove-Item $Filepath;
    Refresh-Env;
}

Function Install-Text-Editor {
    Clear-Host;
    Write-Host "Installing Text Editor...";
    $Filepath = My-Join-Path $VENDOR_DIR ([System.IO.Path]::GetFileName($TEXT_EDITOR_URL));
    Download $TEXT_EDITOR_URL $Filepath;
    Unzip $Filepath $TEXT_EDITOR_DIR;
    Remove-Item $Filepath;
}

Function Unzip ($Zip, $Out) {
    Make-Directory $Out;
    $Shell = New-Object -ComObject Shell.Application;
    $Shell.NameSpace($Out).CopyHere($Shell.NameSpace($Zip).Items());
}

Function Download ($Url, $Filepath) {
    Invoke-WebRequest $Url -OutFile $Filepath;
}

Function Refresh-Env {
    $env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ';' + 
                [System.Environment]::GetEnvironmentVariable("Path","User");
}

function Which ($Command) {
    return Get-Command $Command | Select-Object -ExpandProperty Definition;
}

Function Yes-Or-No ($Prompt, [switch]$Y, [switch]$N) {
    while($true) {
        $Prefer = $Y -xor $N;
        $Answer = Read-Host "$Prompt [$(@('y';'Y')[$Prefer -and $Y.IsPresent])/$(@('n';'N')[$Prefer -and $N.IsPresent])]";
        Switch ($Answer) {
            {$_ -in "y","yes"} {return $true }
            {$_ -in "n","no" } {return $false}
            Default {
                if (!$Answer -and $Prefer) {
                    if ($Y) {return $true }
                    if ($N) {return $false}
                }
                continue;
            }
        }
    }
}

Function Replace-Text ($Filepath, $Find, $Subs) {
    [System.IO.File]::WriteAllLines(
        $Filepath, (
            (Get-Content $Filepath) | 
            Foreach-Object {$_ -replace $Find, $Subs}));
}

Function Make-Directory ($Path) {
    New-Item -Force -Path $Path -Type Directory -ErrorAction SilentlyContinue | Out-Null;
}

Function Remove-Directory ($Path) {
    Remove-Item -Path $Path -Recurse -Force -ErrorAction SilentlyContinue | Out-Null;
}

Function If-Command-Exist ($Command) {
    Get-Command $Command -ErrorAction SilentlyContinue;
}

# Start Calling

# Dev-Clean;

Check-File-Structure;

if (!(Check-Node)) {
    exit;
}

Check-Configs;
Check-User-List;
Check-Node-Modules;

Main-Menu;
